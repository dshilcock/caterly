$('input').iCheck({
	checkboxClass: 'icheckbox_flat-red',
	radioClass   : 'iradio_flat-red'
});

$('#location-enabled').on('ifUnchecked', function(event){
	
});

//Location picker
var map = null;
var autocomplete = null;
var geocoder = null;
var marker = null;

function initMap() {
	var uk = {lat: 46.3578588, lng: -5.3509514};
	var mapOptions = {
			mapTypeId: google.maps.MapTypeId.HYBRID,
			zoom: 10,
			center: uk,
			mapTypeControl: false,
			disableDoubleClickZoom: true,
			zoomControlOptions: true,
			streetViewControl: false
		}
	
	//Load map
	geocoder = new google.maps.Geocoder();
	map = new google.maps.Map(document.getElementById('locationMap'), mapOptions);
	
	//Setup auto complete
	var input = document.getElementById('locationSearch');
	autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.addListener('place_changed', function() {
		 var place = autocomplete.getPlace();
		 if(place.geometry) {
			 setPosition(place.geometry.location);
		 
			 if (place.geometry.viewport) {
				 map.fitBounds(place.geometry.viewport);
			 } else {
				 map.setCenter(place.geometry.location);
				 map.setZoom(10);
			 }
		 }
	});
	
	//Create marker
	marker = new google.maps.Marker({
		position: uk,
		map: map,
		title: "",
		draggable: true
	});
	
	setPositionFromInputs();
	
	//Set listeners
	google.maps.event.addListener(map, 'dblclick', function(event) {
		setPosition(event.latLng);
	});
	
	google.maps.event.addListener(marker, 'dragend', function(event) {
		setPosition(marker.position);
	});
}

//Update map marker when modal is shown
$('#locationModal').on('show.bs.modal', function (e) {
	setPositionFromInputs();
});

$('#locationSave').click(function() {
	var position = marker.getPosition();
	$("#latitude").val(position.lat().toFixed(5));
	$("#longitude").val(position.lng().toFixed(5));
	
	$('#locationModal').modal('hide');
});

$('#locationSearch').focus(function() {
	geolocate();
});

function setPositionFromInputs() {
	var lat = $("#latitude").val();
	var lng = $("#longitude").val();
	
	var position = new google.maps.LatLng(lat, lng);
	setPosition(position);
}

function setPosition(position) {
	marker.setTitle(position.lat().toFixed(5) + ", " + position.lng().toFixed(5));
	marker.setPosition(position);
	map.panTo(position);
};

function geolocate() {
    if (navigator.geolocation) {
    	navigator.geolocation.getCurrentPosition(function(position) {
	    	var geolocation = {
	          lat: position.coords.latitude,
	          lng: position.coords.longitude
	        };
	        
	    	var circle = new google.maps.Circle({
	        	center: geolocation,
	        	radius: position.coords.accuracy
	        });
	    	
	        autocomplete.setBounds(circle.getBounds());
    	});
    }
}