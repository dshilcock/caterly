//Mustache template
var daySection;

$(".add-shift").click(function(e) {
	e.preventDefault();
	
	//Get count of
	var count = $(this).parent().find('.shift-row').length;
	
	if(count < 5) {
		var day = $(this).data('day');
		var type = $(this).data('type');
		
		var rendered = Mustache.render(daySection, {path: ""+type+"."+day+"["+count+"]"});
		$(this).siblings('.shift-rows').append(rendered);
		
		if(count == 4) {
			$(this).prop('disabled', true);
		}
		
		checkboxes();
		
		$('.shift-row-remove').click(function(e) {
			e.preventDefault();
			removeShiftRow(this);
		});
	}
});

$('.shift-row-remove').click(function(e) {
	e.preventDefault();
	removeShiftRow(this);
});

function removeShiftRow(elem) {
	
	var shiftRowsContainer = $(elem).parents('.shift-rows');
	
	if(confirm("Please Confirm")) {
		$(elem).parent().remove();
		
		//rewrite blocks
		$(shiftRowsContainer).find('.shift-row').each(function(index) {
			
			$(this).find('input').each(function() {
				
				var name = $(this).attr('name');
				var currentIdx = parseInt(name.match(/\d+/)[0], 10);
				
				var newName = name.replace(currentIdx, index);
				
				$(this).attr('name', newName);
			});
		});
	}
}

$(function() {
	daySection = $('#daySectionTemplate').html();
	Mustache.parse(daySection);
	
	checkboxes();
});

function checkboxes() {
	
	$('.time_picker').timepicker({
		'timeFormat': 'H:i',
		'forceRoundTime': true,
		'step': 15
	});
	
	$('input').iCheck({
		checkboxClass: 'icheckbox_flat-red',
		radioClass   : 'iradio_flat-red'
	});
}