package com.zentive.naboo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zentive.naboo.db.DBService;

@Controller
public class DashController
{
	@Autowired
	DBService dbService;

	@RequestMapping("/")
	public String dash(Model model)
	{
		model.addAttribute("activeTab", "dash");

		return "dash";
	}
}
