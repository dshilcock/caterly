package com.zentive.naboo.controller.form;

import com.zentive.naboo.model.Location;

public class LocationDeliverySettingsForm
{
	private Boolean deliveryEnabled = false;

	public LocationDeliverySettingsForm(Location location)
	{
		this.deliveryEnabled = location.getDeliveryEnabled();
	}

	public Boolean getDeliveryEnabled()
	{
		return deliveryEnabled;
	}

	public void setDeliveryEnabled(Boolean deliveryEnabled)
	{
		this.deliveryEnabled = deliveryEnabled;
	}
}