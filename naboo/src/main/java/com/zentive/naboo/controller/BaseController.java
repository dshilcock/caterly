package com.zentive.naboo.controller;

import org.springframework.security.core.context.SecurityContextHolder;

import com.zentive.naboo.model.User;

public class BaseController
{
	protected User getCurrentUser()
	{
		User user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof User)
		{
			user = (User) principal;
		}

		return user;
	}
}