package com.zentive.naboo.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zentive.naboo.controller.form.LocationCollectionSettingsForm;
import com.zentive.naboo.controller.form.LocationDeliverySettingsForm;
import com.zentive.naboo.controller.form.LocationSettingsForm;
import com.zentive.naboo.db.DBService;
import com.zentive.naboo.db.exception.DBException;
import com.zentive.naboo.model.Location;
import com.zentive.naboo.model.Shift;
import com.zentive.naboo.model.User;
import com.zentive.naboo.util.shifts.ShiftsModel;

@Controller
public class LocationController extends BaseController
{
	private Logger logger = Logger.getLogger(LocationController.class);

	@Autowired
	private DBService dbService;

	@RequestMapping("/locations")
	public String getLocations(Model model)
	{
		model.addAttribute("activeTab", "locations");

		try
		{
			User currentUser = getCurrentUser();
			model.addAttribute("locations", dbService.locationsForAccount(currentUser.getAccount().getId()));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving locations for account", e);
		}

		return "locations";
	}

	@RequestMapping("/locations/{locationId}")
	public String getLocation(Model model, @PathVariable Long locationId)
	{
		model.addAttribute("activeTab", "locations");
		model.addAttribute("locationActive", "overview");

		try
		{
			User currentUser = getCurrentUser();
			model.addAttribute("location", dbService.locationForAccount(currentUser.getAccount().getId(), locationId));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving location", e);
		}

		return "location";
	}

	@RequestMapping("/locations/{locationId}/settings")
	public String getLocationSettings(Model model, @PathVariable Long locationId)
	{
		model.addAttribute("activeTab", "locations");
		model.addAttribute("locationActive", "settings");

		try
		{
			User currentUser = getCurrentUser();
			Location location = dbService.locationForAccount(currentUser.getAccount().getId(), locationId);

			model.addAttribute("location", location);
			model.addAttribute("locationSettingsForm", new LocationSettingsForm(location));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving location", e);
		}

		return "locationSettings";
	}

	@RequestMapping("/locations/{locationId}/collection")
	public String getLocationCollectionSettings(Model model, @PathVariable Long locationId)
	{
		model.addAttribute("activeTab", "locations");
		model.addAttribute("locationActive", "collection");

		try
		{
			User currentUser = getCurrentUser();
			Location location = dbService.locationForAccount(currentUser.getAccount().getId(), locationId);

			model.addAttribute("location", location);
			model.addAttribute("locationCollectionSettingsForm", new LocationCollectionSettingsForm(location));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving location", e);
		}

		return "locationCollectionSettings";
	}

	@RequestMapping("/locations/{locationId}/delivery")
	public String getLocationDeliverySettings(Model model, @PathVariable Long locationId)
	{
		model.addAttribute("activeTab", "locations");
		model.addAttribute("locationActive", "delivery");

		try
		{
			User currentUser = getCurrentUser();
			Location location = dbService.locationForAccount(currentUser.getAccount().getId(), locationId);

			model.addAttribute("location", location);
			model.addAttribute("locationDeliverySettingsForm", new LocationDeliverySettingsForm(location));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving location", e);
		}

		return "locationDeliverySettings";
	}

	@RequestMapping("/locations/{locationId}/hours")
	public String getLocationHoursSettings(Model model, @PathVariable Long locationId)
	{
		model.addAttribute("activeTab", "locations");
		model.addAttribute("locationActive", "hours");

		try
		{
			User currentUser = getCurrentUser();
			Location location = dbService.locationForAccount(currentUser.getAccount().getId(), locationId);

			if (location != null)
			{
				List<Shift> shifts = dbService.shiftsForLocation(locationId);

				// Build shifts model
				ShiftsModel shiftsModel = new ShiftsModel(shifts);
				model.addAttribute("shiftsModel", shiftsModel);
			}

			model.addAttribute("location", location);
		}
		catch (DBException e)
		{
			logger.error("Error retrieving location", e);
		}

		return "locationHoursSettings";
	}

	@RequestMapping(value = "/locations/{locationId}/hours", method = RequestMethod.POST)
	public String updateLocationHoursSettings(@PathVariable Long locationId, @Valid @ModelAttribute ShiftsModel shiftsModel, BindingResult result)
	{
		String redirect = "redirect:/locations";

		try
		{
			User currentUser = getCurrentUser();
			Location location = dbService.locationForAccount(currentUser.getAccount().getId(), locationId);
			if (location != null)
			{
				if (!result.hasErrors())
				{
					dbService.updateOpeningHours(currentUser.getAccount(), location, shiftsModel);

					redirect = "redirect:/locations/" + locationId + "/hours";
				}
				else
				{
					// Set redirect message
				}
			}
			else
			{
				// Location not found
			}
		}
		catch (DBException e)
		{
			logger.error("Error updating location hours", e);
		}

		return redirect;
	}
}
