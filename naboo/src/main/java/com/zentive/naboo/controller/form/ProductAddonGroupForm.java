package com.zentive.naboo.controller.form;

import java.util.List;

public class ProductAddonGroupForm
{
	private Long id;
	private String title;
	private List<ProductAddonForm> productAddons;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public List<ProductAddonForm> getProductAddons()
	{
		return productAddons;
	}

	public void setProductAddons(List<ProductAddonForm> productAddons)
	{
		this.productAddons = productAddons;
	}
}