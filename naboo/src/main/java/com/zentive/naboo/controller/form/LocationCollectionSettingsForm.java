package com.zentive.naboo.controller.form;

import com.zentive.naboo.model.Location;

public class LocationCollectionSettingsForm
{
	private Boolean collectionEnabled = false;

	public LocationCollectionSettingsForm(Location location)
	{
		this.collectionEnabled = location.getCollectionEnabled();
	}

	public Boolean getCollectionEnabled()
	{
		return collectionEnabled;
	}

	public void setCollectionEnabled(Boolean collectionEnabled)
	{
		this.collectionEnabled = collectionEnabled;
	}
}