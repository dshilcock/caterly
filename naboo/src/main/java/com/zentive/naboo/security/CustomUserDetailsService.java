package com.zentive.naboo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.zentive.naboo.db.DBService;
import com.zentive.naboo.db.exception.DBException;

@Service
public class CustomUserDetailsService implements UserDetailsService
{
	@Autowired
	private DBService dbService;

	public CustomUserDetailsService(DBService dbService)
	{
		this.dbService = dbService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		try
		{
			return dbService.userByUsername(username);
		}
		catch (DBException e)
		{
			throw new AuthenticationServiceException("Error retrieving user", e);
		}
	}

	public DBService getDbService()
	{
		return dbService;
	}

	public void setDbService(DBService dbService)
	{
		this.dbService = dbService;
	}
}