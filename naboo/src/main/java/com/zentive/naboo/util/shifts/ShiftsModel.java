package com.zentive.naboo.util.shifts;

import java.util.List;

import javax.validation.Valid;

import org.joda.time.DateTime;

import com.zentive.naboo.model.Shift;

public class ShiftsModel
{
	@Valid
	private ShiftTypeModel collectionShifts = new ShiftTypeModel();

	@Valid
	private ShiftTypeModel deliveryShifts = new ShiftTypeModel();

	public ShiftsModel()
	{

	}

	public ShiftsModel(List<Shift> shifts)
	{
		if (shifts != null)
		{
			for (Shift shift : shifts)
			{
				if (shift.getIsDelivery())
				{
					deliveryShifts.addShift(shift);
				}
				else
				{
					collectionShifts.addShift(shift);
				}
			}

			collectionShifts.sort();
			deliveryShifts.sort();
		}
	}

	public Boolean collectionOpenAt(DateTime date)
	{
		return collectionShifts.openAt(date);
	}

	public ShiftTypeModel getCollectionShifts()
	{
		return collectionShifts;
	}

	public void setCollectionShifts(ShiftTypeModel collectionShifts)
	{
		this.collectionShifts = collectionShifts;
	}

	public ShiftTypeModel getDeliveryShifts()
	{
		return deliveryShifts;
	}

	public void setDeliveryShifts(ShiftTypeModel deliveryShifts)
	{
		this.deliveryShifts = deliveryShifts;
	}
}