package com.zentive.naboo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Menu
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@Column(nullable = false)
	private Boolean enabled = false;

	@Column(nullable = false)
	private String name;

	@Column(nullable = true)
	private String description;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "menu")
	private List<LocationMenu> locationMenus;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "menu")
	private List<MenuCategory> categories;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Account account;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Boolean getEnabled()
	{
		return enabled;
	}

	public void setEnabled(Boolean enabled)
	{
		this.enabled = enabled;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<LocationMenu> getLocationMenus()
	{
		return locationMenus;
	}

	public void setLocationMenus(List<LocationMenu> locationMenus)
	{
		this.locationMenus = locationMenus;
	}

	public Account getAccount()
	{
		return account;
	}

	public void setAccount(Account account)
	{
		this.account = account;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
