package com.zentive.naboo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Location
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private Boolean enabled = false;

	@Column(nullable = false)
	private Boolean deliveryEnabled = false;

	@Column(nullable = false)
	private Boolean collectionEnabled = false;

	@Column(nullable = true)
	private String address;

	@Column(nullable = true)
	private Double latitude;

	@Column(nullable = true)
	private Double longitude;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Account account;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
	private List<LocationMenu> locationMenus;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
	private List<DeliveryZone> deliveryZones;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
	private List<Shift> shifts;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Boolean getEnabled()
	{
		return enabled;
	}

	public void setEnabled(Boolean enabled)
	{
		this.enabled = enabled;
	}

	public Account getAccount()
	{
		return account;
	}

	public void setAccount(Account account)
	{
		this.account = account;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public List<DeliveryZone> getDeliveryZones()
	{
		return deliveryZones;
	}

	public void setDeliveryZones(List<DeliveryZone> deliveryZones)
	{
		this.deliveryZones = deliveryZones;
	}

	public Boolean getDeliveryEnabled()
	{
		return deliveryEnabled;
	}

	public void setDeliveryEnabled(Boolean deliveryEnabled)
	{
		this.deliveryEnabled = deliveryEnabled;
	}

	public Boolean getCollectionEnabled()
	{
		return collectionEnabled;
	}

	public void setCollectionEnabled(Boolean collectionEnabled)
	{
		this.collectionEnabled = collectionEnabled;
	}

	public List<LocationMenu> getLocationMenus()
	{
		return locationMenus;
	}

	public void setLocationMenus(List<LocationMenu> locationMenus)
	{
		this.locationMenus = locationMenus;
	}

	public Double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(Double latitude)
	{
		this.latitude = latitude;
	}

	public Double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(Double longitude)
	{
		this.longitude = longitude;
	}
}
