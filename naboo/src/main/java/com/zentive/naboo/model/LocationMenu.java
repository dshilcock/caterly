package com.zentive.naboo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class LocationMenu
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Location location;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Menu menu;

	@Column(nullable = false)
	private Boolean enableDelivery = false;

	@Column(nullable = false)
	private Boolean enableCollection = false;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public Menu getMenu()
	{
		return menu;
	}

	public void setMenu(Menu menu)
	{
		this.menu = menu;
	}

	public Boolean getEnableDelivery()
	{
		return enableDelivery;
	}

	public void setEnableDelivery(Boolean enableDelivery)
	{
		this.enableDelivery = enableDelivery;
	}

	public Boolean getEnableCollection()
	{
		return enableCollection;
	}

	public void setEnableCollection(Boolean enableCollection)
	{
		this.enableCollection = enableCollection;
	}
}
