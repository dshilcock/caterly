package com.zentive.naboo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Shift
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private Boolean isDelivery = false;

	@Column(nullable = false)
	private Integer dayOfWeek;

	@Column(nullable = false)
	private Integer startHour;

	@Column(nullable = false)
	private Integer startMinutes;

	@Column(nullable = false)
	private Integer endHour;

	@Column(nullable = false)
	private Integer endMinutes;

	@Column(nullable = false)
	private Boolean enabled = true;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Location location;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public Boolean getIsDelivery()
	{
		return isDelivery;
	}

	public void setIsDelivery(Boolean isDelivery)
	{
		this.isDelivery = isDelivery;
	}

	public Integer getDayOfWeek()
	{
		return dayOfWeek;
	}

	public void setDayOfWeek(Integer dayOfWeek)
	{
		this.dayOfWeek = dayOfWeek;
	}

	public Boolean getEnabled()
	{
		return enabled;
	}

	public void setEnabled(Boolean enabled)
	{
		this.enabled = enabled;
	}

	public Integer getStartHour()
	{
		return startHour;
	}

	public void setStartHour(Integer startHour)
	{
		this.startHour = startHour;
	}

	public Integer getEndHour()
	{
		return endHour;
	}

	public void setEndHour(Integer endHour)
	{
		this.endHour = endHour;
	}

	public Integer getStartMinutes()
	{
		return startMinutes;
	}

	public void setStartMinutes(Integer startMinutes)
	{
		this.startMinutes = startMinutes;
	}

	public Integer getEndMinutes()
	{
		return endMinutes;
	}

	public void setEndMinutes(Integer endMinutes)
	{
		this.endMinutes = endMinutes;
	}
}