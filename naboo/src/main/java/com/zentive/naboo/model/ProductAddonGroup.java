package com.zentive.naboo.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.zentive.naboo.controller.form.ProductAddonForm;
import com.zentive.naboo.controller.form.ProductAddonGroupForm;

@Entity
public class ProductAddonGroup
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Product product;

	@Column(nullable = false)
	private String title;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "productAddonGroup")
	private Set<ProductAddon> productAddons = null;

	public ProductAddonGroupForm toForm()
	{
		ProductAddonGroupForm form = new ProductAddonGroupForm();

		form.setId(id);
		form.setTitle(title);

		List<ProductAddonForm> productAddonForms = new ArrayList<ProductAddonForm>();

		for (ProductAddon productAddon : productAddons)
		{
			productAddonForms.add(productAddon.toForm());
		}

		form.setProductAddons(productAddonForms);

		return form;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Product getProduct()
	{
		return product;
	}

	public void setProduct(Product product)
	{
		this.product = product;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Set<ProductAddon> getProductOptions()
	{
		return productAddons;
	}

	public void setProductOptions(Set<ProductAddon> productAddons)
	{
		this.productAddons = productAddons;
	}
}
