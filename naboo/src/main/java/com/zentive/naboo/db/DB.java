package com.zentive.naboo.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.zentive.naboo.db.exception.DBException;

@Repository
public class DB
{
	private static SessionFactory sessionFactory;

	public DB(SessionFactory sessionFactory)
	{
		DB.sessionFactory = sessionFactory;
	}
	
	public void delete(Session session, Object object) throws DBException
	{
		session.delete(object);
	}

	public void delete(final Object object) throws DBException
	{
		Session session = null;
		try
		{
			session = writeSession();
			final Transaction t = session.beginTransaction();
			session.delete(object);
			t.commit();
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not delete object", (Throwable) e);
		}
		finally
		{
			closeSession(session);
		}
	}

	public void saveOrUpdate(Session session, Object object) throws DBException
	{
		session.saveOrUpdate(object);
	}

	public void insert(Session session, Object object) throws DBException
	{
		session.save(object);
	}

	public void insert(final Object object) throws DBException
	{
		Session session = null;
		try
		{
			session = writeSession();
			final Transaction t = session.beginTransaction();
			session.save(object);
			t.commit();
		}
		catch (HibernateException e)
		{
			throw new DBException("Cound not insert object", (Throwable) e);
		}
		finally
		{
			closeSession(session);
		}
	}

	public void update(Session session, final Object object) throws DBException
	{
		session.update(object);
	}

	public void update(final Object object) throws DBException
	{
		Session session = null;
		try
		{
			session = writeSession();
			final Transaction t = session.beginTransaction();
			session.update(object);
			t.commit();
		}
		catch (HibernateException e)
		{
			throw new DBException("Cound not update object", (Throwable) e);
		}
		finally
		{
			closeSession(session);
		}
	}

	public <T> T refresh(T object) throws DBException
	{
		Session session = null;
		try
		{
			session = readSession();
			session.refresh(object);

			return object;
		}
		catch (HibernateException e)
		{
			throw new DBException("Cound not update object", (Throwable) e);
		}
		finally
		{
			closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T objectById(Class<T> clazz, Long id) throws DBException
	{
		Session session = null;
		try
		{
			session = readSession();
			return (T) session.createCriteria(clazz).add(Restrictions.idEq(id)).uniqueResult();
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not retrieve object by id", e);
		}
		finally
		{
			closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T objectById(Session session, Class<T> clazz, Long id) throws DBException
	{
		try
		{
			return (T) session.createCriteria(clazz).add(Restrictions.idEq(id)).uniqueResult();
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not retrieve object by id", e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T objectForCriteria(Class<T> clazz, DetachedCriteria detachedCriteria) throws DBException
	{
		Session session = null;
		try
		{
			session = readSession();
			Criteria c = detachedCriteria.getExecutableCriteria(session);

			return (T) c.uniqueResult();
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not retrieve object by id", e);
		}
		finally
		{
			closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T objectForCriteria(Session session, Class<T> clazz, DetachedCriteria detachedCriteria) throws DBException
	{
		Criteria c = detachedCriteria.getExecutableCriteria(session);
		c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return (T) c.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> listForCriteria(Class<T> clazz, DetachedCriteria detachedCriteria) throws DBException
	{
		Session session = null;
		try
		{
			session = readSession();
			Criteria c = detachedCriteria.getExecutableCriteria(session);

			return (List<T>) c.list();
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not retrieve list by criteria", e);
		}
		finally
		{
			closeSession(session);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> list(Class<T> clazz) throws DBException
	{
		Session session = null;
		try
		{
			session = readSession();
			Criteria c = session.createCriteria(clazz);

			return (List<T>) c.list();
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not retrieve list by criteria", e);
		}
		finally
		{
			closeSession(session);
		}
	}

	public <T> Integer countForCriteria(Class<T> clazz, DetachedCriteria detachedCriteria) throws DBException
	{
		Session session = null;
		try
		{
			session = readSession();
			Criteria c = detachedCriteria.getExecutableCriteria(session).setProjection(Projections.rowCount());

			Number number = (Number) c.uniqueResult();
			if (number != null)
			{
				return number.intValue();
			}
			else
			{
				return null;
			}
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not retrieve list by criteria", e);
		}
		finally
		{
			closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> listForCriteria(Class<T> clazz, DetachedCriteria detachedCriteria, Integer limit, Integer first) throws DBException
	{
		Session session = null;
		try
		{
			session = readSession();
			Criteria c = detachedCriteria.getExecutableCriteria(session);

			if (limit != null && limit > 0)
			{
				c.setMaxResults(limit);
			}

			if (first >= 0)
			{
				c.setFirstResult(first);
			}

			return (List<T>) c.list();
		}
		catch (HibernateException e)
		{
			throw new DBException("Could not retrieve list by criteria", e);
		}
		finally
		{
			closeSession(session);
		}
	}
	
	public Session writeSession()
	{
		return DB.sessionFactory.openSession();
	}

	public Session readSession()
	{
		Session session = DB.sessionFactory.openSession();
		session.setDefaultReadOnly(true);

		return session;
	}

	public void closeSession(final Session session)
	{
		if (session != null)
		{
			session.close();
		}
	}
	
	public SessionFactory getSessionFactory() 
	{
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) 
	{
		DB.sessionFactory = sessionFactory;
	}
}