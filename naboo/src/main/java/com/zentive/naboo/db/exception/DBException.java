package com.zentive.naboo.db.exception;

public class DBException extends Exception 
{
	private static final long serialVersionUID = 7344511992169054636L;
	
	public DBException(String message)
	{
		super(message);
	}
	
	public DBException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
