package com.zentive.naboo.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.zentive.naboo.controller.form.ProductForm;

@Entity
public class Product
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Account account;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private MenuCategory menuCategory;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	private List<ProductOption> productOptions = null;

	@Column(nullable = false)
	private String name;

	@Column(nullable = true)
	private String displayName;

	@Column(nullable = true, length = 500)
	private String description;

	@Column(nullable = false)
	private Integer price;

	@Column
	private Boolean active;

	// Options

	// Add-ons

	public Product()
	{
		// Default
	}

	public Product(ProductForm productForm)
	{
		this.name = productForm.getName();
		this.displayName = productForm.getDisplayName();
		this.description = productForm.getDescription();
		this.active = productForm.getActive() != null && productForm.getActive() ? true : false;
		this.price = Product.decimalToInteger(productForm.getPrice());
	}

	public ProductForm toForm()
	{
		ProductForm form = new ProductForm();
		form.setId(id);
		form.setName(name);
		form.setDisplayName(displayName);
		form.setActive(active);
		form.setPrice(Product.integerToDecimal(price));

		return form;
	}

	public static Integer decimalToInteger(BigDecimal bigDecimal)
	{
		BigDecimal bigDecimalInCents = bigDecimal.movePointRight(2);
		return bigDecimalInCents.intValueExact();
	}

	public static BigDecimal integerToDecimal(Integer integer)
	{
		BigDecimal bigDecimal = new BigDecimal(integer).movePointLeft(2);
		return bigDecimal;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public MenuCategory getMenuCategory()
	{
		return menuCategory;
	}

	public void setMenuCategory(MenuCategory menuCategory)
	{
		this.menuCategory = menuCategory;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getPrice()
	{
		return price;
	}

	public BigDecimal getPriceAsDecimal()
	{
		return Product.integerToDecimal(price);
	}

	public void setPrice(Integer price)
	{
		this.price = price;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public Boolean getActive()
	{
		return active;
	}

	public void setActive(Boolean active)
	{
		this.active = active;
	}

	public Account getAccount()
	{
		return account;
	}

	public void setAccount(Account account)
	{
		this.account = account;
	}
}
