package com.zentive.naboo.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.zentive.naboo.controller.form.ProductOptionGroupForm;

@Entity
public class ProductOptionGroup
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Product product;

	@Column(nullable = false)
	private String title;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "productOptionGroup")
	private Set<ProductOption> productOptions = null;

	public ProductOptionGroupForm toForm()
	{
		ProductOptionGroupForm form = new ProductOptionGroupForm();

		form.setId(id);
		form.setTitle(title);
		
		List<ProductOptionForm> productOptionForms = new ArrayList<ProductOptionForm>();
		
		form.setProductOptions(productOptionForms);
		
		return form;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Product getProduct()
	{
		return product;
	}

	public void setProduct(Product product)
	{
		this.product = product;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Set<ProductOption> getProductOptions()
	{
		return productOptions;
	}

	public void setProductOptions(Set<ProductOption> productOptions)
	{
		this.productOptions = productOptions;
	}
}
