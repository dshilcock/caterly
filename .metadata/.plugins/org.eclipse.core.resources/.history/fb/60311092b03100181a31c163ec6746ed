package com.zentive.naboo.util.shifts;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.zentive.naboo.model.Shift;

public class ShiftModel
{
	private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");

	private Boolean enabled;

	@NotNull
	private String startTime;

	@NotNull
	private String endTime;

	public ShiftModel()
	{

	}

	public ShiftModel(Shift shift)
	{
		this.enabled = shift.getEnabled();

		LocalTime startTime = new LocalTime(shift.getStartHour(), shift.getStartMinutes(), 0);
		LocalTime endTime = new LocalTime(shift.getEndHour(), shift.getEndMinutes(), 0);

		this.startTime = shift.getStartHour() + ":" + shift.getStartMinutes();
		this.endTime = shift.getEndHour() + ":" + shift.getEndMinutes();
	}

	@AssertTrue(message = "Start and end time invalid")
	public boolean isOk()
	{
		// Custom validation here
		return startTime != null && endTime != null;
	}

	public LocalTime getStartTimeAsLocalTime()
	{
		return fmt.parseLocalTime(getStartTime());
	}

	public LocalTime getEndTimeAsLocalTime()
	{
		return fmt.parseLocalTime(getEndTime());
	}

	public Boolean getEnabled()
	{
		return enabled;
	}

	public void setEnabled(Boolean enabled)
	{
		this.enabled = enabled;
	}

	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}
}