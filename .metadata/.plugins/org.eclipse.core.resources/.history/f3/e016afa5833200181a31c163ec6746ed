package com.zentive.naboo.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zentive.naboo.controller.form.ProductForm;
import com.zentive.naboo.db.DBService;
import com.zentive.naboo.db.exception.DBException;
import com.zentive.naboo.model.Product;
import com.zentive.naboo.model.User;

@Controller
public class ProductController extends BaseController
{
	private Logger logger = Logger.getLogger(ProductController.class);

	@Autowired
	private DBService dbService;

	@RequestMapping(value = "/products/{productId}", method = RequestMethod.GET)
	public String getProduct(HttpServletResponse response, Model model, @PathVariable Long productId)
	{
		try
		{
			User currentUser = getCurrentUser();
			model.addAttribute("product", dbService.getProductForAccount(currentUser.getAccount().getId(), productId));
		}
		catch (DBException e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/products/{productId}/update", method = RequestMethod.POST)
	public void updateProduct(HttpServletResponse response, @PathVariable Long productId, @Valid @ModelAttribute ProductForm productForm, BindingResult result)
	{
		try
		{
			User currentUser = getCurrentUser();
			Product product = dbService.getProductForAccount(currentUser.getAccount().getId(), productId);
			if (product != null)
			{
				// Update the product
				product.setName(productForm.getName());
				product.setDisplayName(productForm.getDisplayName());
				product.setDescription(productForm.getDescription());
				product.setPrice(Product.decimalToInteger(productForm.getPrice()));

				// Save the product
				dbService.getDB().update(product);
			}
			else
			{
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		}
		catch (DBException e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
}
