<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:th="http://www.thymeleaf.org" 
  xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" 
  layout:decorator="template">
  
  	<head>
	    <title>Location - Opening Hours</title>
	    
	    <th:block layout:fragment="styles">
	    <link th:href="@{/assets/plugins/icheck/skins/flat/red.css}" rel="stylesheet"/>
	    <link th:href="@{/assets/plugins/jquery-timepicker/jquery.timepicker.min.css}" rel="stylesheet"/>
	    </th:block>
  	</head>
  
 	<div layout:fragment="page_header_title" id="page_header_title">
		<span th:text="${location.name}"></span>
	</div>
  
	<div layout:fragment="page_content" id="page_content">
  		<div th:replace="locationSubmenu"></div>
  		<div class="row">
  			<div class="col-lg-12">
  				<div class="pvr-wrapper">
  					<div class="pvr-box">
  						<h5 class="pvr-header">
							Opening Hours
							<div class="pvr-box-controls">
								<i class="material-icons" data-box="fullscreen">fullscreen</i>
							</div>
						</h5>
						<div id="opening-hours" role="tabpanel">
							<form method="post" th:action="@{'/locations/' + ${location.id} + '/hours'}" th:object="${shiftsModel}">
		                        <nav class="nav">
		                            <a class="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#collection">Collection</a>
		                            <a class="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#delivery">Delivery</a>
		                            
		                            <div style="position: absolute; right: 50px;">
		                            	<button class="btn btn-sm btn-primary" type="submit">Save</button>
		                            	<button class="btn btn-sm btn-outline-primary">Cancel</button>
		                            </div>
		                        </nav>

								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active show" id="collection" aria-labelledby="home-tab" aria-expanded="true">
										<div class="shift-day pb-3">
											<h5>Monday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.collectionShifts.mon}">
													<input class="form-control time_picker mr-3" type="text" th:field="*{collectionShifts.mon[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-3 mr-3" type="text" th:field="*{collectionShifts.mon[__${i.index}__].endTime}" style="display: inline;"/>
													<span>for</span>
													<select class="form-control ml-3 mr-3" style="display: inline;">
														<option value="1">Breakfast</option>
														<option value="2">Lunch</option>
														<option value="3">Dinner</option>
													</select>
													<input type="checkbox" th:field="*{collectionShifts.mon[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="mon" data-type="collectionShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Tuesday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.collectionShifts.tue}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{collectionShifts.tue[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{collectionShifts.tue[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{collectionShifts.tue[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="tue" data-type="collectionShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Wednesday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.collectionShifts.wed}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{collectionShifts.wed[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{collectionShifts.wed[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{collectionShifts.wed[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="wed" data-type="collectionShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Thursday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.collectionShifts.thu}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{collectionShifts.thu[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{collectionShifts.thu[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{collectionShifts.thu[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="thu" data-type="collectionShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Friday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.collectionShifts.fri}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{collectionShifts.fri[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{collectionShifts.fri[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{collectionShifts.fri[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="fri" data-type="collectionShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Saturday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.collectionShifts.sat}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{collectionShifts.sat[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{collectionShifts.sat[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{collectionShifts.sat[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="sat" data-type="collectionShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Sunday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.collectionShifts.sun}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{collectionShifts.sun[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{collectionShifts.sun[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{collectionShifts.sun[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="sun" data-type="collectionShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
									</div>
									
									<div role="tabpanel" class="tab-pane" id="delivery" aria-labelledby="home-tab" aria-expanded="true">
										<div class="shift-day pb-3">
											<h5>Monday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.deliveryShifts.mon}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{deliveryShifts.mon[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{deliveryShifts.mon[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{deliveryShifts.mon[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="mon" data-type="deliveryShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Tuesday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.deliveryShifts.tue}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{deliveryShifts.tue[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{deliveryShifts.tue[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{deliveryShifts.tue[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="tue" data-type="deliveryShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Wednesday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.deliveryShifts.wed}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{deliveryShifts.wed[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{deliveryShifts.wed[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{deliveryShifts.wed[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="wed" data-type="deliveryShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Thursday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.deliveryShifts.thu}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{deliveryShifts.thu[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{deliveryShifts.thu[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{deliveryShifts.thu[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="thu" data-type="deliveryShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Friday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.deliveryShifts.fri}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{deliveryShifts.fri[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{deliveryShifts.fri[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{deliveryShifts.fri[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="fri" data-type="deliveryShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Saturday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.deliveryShifts.sat}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{deliveryShifts.sat[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{deliveryShifts.sat[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{deliveryShifts.sat[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="sat" data-type="deliveryShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
										
										<div class="shift-day pb-3">
											<h5>Sunday</h5>
											<div class="shift-rows">
												<div class="shift-row" th:each="shiftDay,i : ${shiftsModel.deliveryShifts.sun}">
													<input class="form-control time_picker mr-2" type="text" th:field="*{deliveryShifts.sun[__${i.index}__].startTime}" style="display: inline;"/>
													<span>to</span>
													<input class="form-control time_picker ml-2 mr-2" type="text" th:field="*{deliveryShifts.sun[__${i.index}__].endTime}" style="display: inline;"/>
													<input type="checkbox" th:field="*{deliveryShifts.sun[__${i.index}__].enabled}"/><label class="ml-1">Active</label>
													<button class="shift-row-remove ml-1 btn btn-sm">
					                                    <i class="material-icons align-middle">delete</i>
					                                </button>
												</div>
											</div>
											<button data-day="sun" data-type="deliveryShifts" class="add-shift btn btn-sm btn-dark">Add</button>
										</div>
									</div>
								</div>
							</form>
						</div>
  					</div>
  				</div>
  			</div>
  		</div>
	</div>
<th:block layout:fragment="scripts">
<script id="daySectionTemplate" type="x-tmpl-mustache">
<div class='shift-row'>
	<input name="{{path}}.startTime" class="form-control time_picker mr-2" type="text" style="display: inline;"/>
	<span>to</span>
	<input name="{{path}}.endTime" class="form-control time_picker ml-2 mr-2" type="text" style="display: inline;"/>
	<input name="{{path}}.enabled" checked="checked" type="checkbox"/><label class="ml-1">Active</label>
	<button class="shift-row-remove ml-1 btn btn-sm">
		<i class="material-icons align-middle">delete</i>
	</button>
</div>
</script>
<script th:src="@{/assets/plugins/icheck/icheck.min.js}"></script>
<script th:src="@{/assets/plugins/mustache/mustache.min.js}"></script>
<script th:src="@{/assets/plugins/jquery-timepicker/jquery.timepicker.min.js}"></script>
<script th:src="@{/assets/js/locationHoursSettings.js}"></script>
</th:block>
</html>