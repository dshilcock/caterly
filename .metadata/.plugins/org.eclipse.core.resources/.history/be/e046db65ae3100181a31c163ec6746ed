package com.zentive.naboo.db;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.zentive.naboo.db.exception.DBException;
import com.zentive.naboo.model.Account;
import com.zentive.naboo.model.Location;
import com.zentive.naboo.model.Menu;
import com.zentive.naboo.model.MenuCategory;
import com.zentive.naboo.model.Shift;
import com.zentive.naboo.model.User;
import com.zentive.naboo.util.shifts.ShiftModel;
import com.zentive.naboo.util.shifts.ShiftTypeModel;
import com.zentive.naboo.util.shifts.ShiftsModel;

@Service
public class DBService
{
	private DB db;

	// Menus
	public List<Menu> menusForAccount(Long accountId) throws DBException
	{
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Menu.class);
		detachedCriteria.add(Restrictions.eq("account.id", accountId));

		return db.listForCriteria(Menu.class, detachedCriteria);
	}

	public List<Menu> menuForAccount(Long accountId, Long menuId) throws DBException
	{
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Menu.class);
		detachedCriteria.add(Restrictions.eq("account.id", accountId));
		detachedCriteria.add(Restrictions.idEq(menuId));

		return db.listForCriteria(Menu.class, detachedCriteria);
	}

	public List<MenuCategory> menuCategories(Long accountId, Long menuId) throws DBException
	{
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(MenuCategory.class)
				.add(Restrictions.eq("menu.id", menuId))
				.createCriteria("menu")
				.createCriteria("account")
				.add(Restrictions.idEq(accountId));
				
		

		return db.listForCriteria(MenuCategory.class, detachedCriteria);
	}

	// Shift

	public List<Shift> shiftsForLocation(Long locationId) throws DBException
	{
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Shift.class);
		detachedCriteria.add(Restrictions.eq("location.id", locationId));

		return db.listForCriteria(Shift.class, detachedCriteria);
	}

	// Location

	public List<Location> locationsForAccount(Long accountId) throws DBException
	{
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Location.class);
		detachedCriteria.add(Restrictions.eq("account.id", accountId));

		return db.listForCriteria(Location.class, detachedCriteria);
	}

	public Location locationForAccount(Long accountId, Long locationId) throws DBException
	{
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Location.class);
		detachedCriteria.add(Restrictions.eq("account.id", accountId));
		detachedCriteria.add(Restrictions.idEq(locationId));

		return db.objectForCriteria(Location.class, detachedCriteria);
	}

	// User

	public User userByUsername(String username) throws DBException
	{
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Restrictions.eq("username", username));
		detachedCriteria.setFetchMode("account", FetchMode.JOIN);

		return db.objectForCriteria(User.class, detachedCriteria);
	}

	public void updateOpeningHours(Account account, Location location, ShiftsModel shiftsModel) throws DBException
	{
		Session session = null;

		try
		{
			session = db.writeSession();
			Transaction t = session.beginTransaction();

			// Get account for update - to lock it and prevent reads / writes while we update the opening hours
			session.refresh(account, LockMode.PESSIMISTIC_WRITE);

			// Delete the existing shifts
			Query deleteShiftsQuery = session.createQuery("delete Shift where location = :location");
			deleteShiftsQuery.setEntity("location", location);
			deleteShiftsQuery.executeUpdate();

			// Add new shifts from model
			List<Shift> collectionShifts = createShiftsFromTypeModel(location, shiftsModel.getCollectionShifts(), false);
			for (Shift shift : collectionShifts)
			{
				session.save(shift);
			}

			List<Shift> deliveryShifts = createShiftsFromTypeModel(location, shiftsModel.getDeliveryShifts(), true);
			for (Shift shift : deliveryShifts)
			{
				session.save(shift);
			}

			t.commit();
		}
		catch (HibernateException e)
		{
			e.printStackTrace();
		}
		finally
		{
			db.closeSession(session);
		}
	}

	private List<Shift> createShiftsFromTypeModel(Location location, ShiftTypeModel shiftTypeModel, Boolean isDelivery)
	{
		List<Shift> shifts = new ArrayList<Shift>();

		shifts.addAll(createShiftsFromModel(location, 1, isDelivery, shiftTypeModel.getMon()));
		shifts.addAll(createShiftsFromModel(location, 2, isDelivery, shiftTypeModel.getTue()));
		shifts.addAll(createShiftsFromModel(location, 3, isDelivery, shiftTypeModel.getWed()));
		shifts.addAll(createShiftsFromModel(location, 4, isDelivery, shiftTypeModel.getThu()));
		shifts.addAll(createShiftsFromModel(location, 5, isDelivery, shiftTypeModel.getFri()));
		shifts.addAll(createShiftsFromModel(location, 6, isDelivery, shiftTypeModel.getSat()));
		shifts.addAll(createShiftsFromModel(location, 7, isDelivery, shiftTypeModel.getSun()));

		return shifts;
	}

	private List<Shift> createShiftsFromModel(Location location, Integer day, Boolean isDelivery, List<ShiftModel> shiftModels)
	{
		List<Shift> shifts = new ArrayList<Shift>();

		for (ShiftModel shiftModel : shiftModels)
		{
			Shift shift = new Shift();
			shift.setDayOfWeek(day);
			shift.setStartHour(shiftModel.getStartTimeAsLocalTime().getHourOfDay());
			shift.setEndHour(shiftModel.getEndTimeAsLocalTime().getHourOfDay());
			shift.setEnabled(shiftModel.getEnabled() != null ? shiftModel.getEnabled() : false);
			shift.setIsDelivery(isDelivery);
			shift.setLocation(location);

			shifts.add(shift);
		}

		return shifts;
	}

	public DBService(DB db)
	{
		this.db = db;
	}

	public DB getDB()
	{
		return db;
	}

}
