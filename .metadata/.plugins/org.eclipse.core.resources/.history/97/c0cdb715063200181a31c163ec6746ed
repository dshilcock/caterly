package com.zentive.naboo.controller;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zentive.naboo.controller.form.ProductForm;
import com.zentive.naboo.db.DBService;
import com.zentive.naboo.db.exception.DBException;
import com.zentive.naboo.model.Menu;
import com.zentive.naboo.model.User;

@Controller
public class MenuController extends BaseController
{
	private Logger logger = Logger.getLogger(MenuController.class);

	@Autowired
	private DBService dbService;

	@RequestMapping("/menus")
	public String getMenus(Model model)
	{
		model.addAttribute("activeTab", "menus");

		try
		{
			User currentUser = getCurrentUser();
			model.addAttribute("menus", dbService.menusForAccount(currentUser.getAccount().getId()));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving menus for account", e);
		}

		return "menus";
	}

	@RequestMapping("/menus/{menuId}")
	public String getMenu(Model model, @PathVariable Long menuId)
	{
		model.addAttribute("activeTab", "menus");

		try
		{
			User currentUser = getCurrentUser();
			model.addAttribute("menu", dbService.menuForAccount(currentUser.getAccount().getId(), menuId));
			model.addAttribute("categories", dbService.menuCategories(currentUser.getAccount().getId(), menuId));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving menu for account", e);
		}

		return "menu";
	}

	@RequestMapping("/menus/{menuId}/content")
	public String getMenuContent(Model model, @PathVariable Long menuId)
	{
		try
		{
			User currentUser = getCurrentUser();
			model.addAttribute("menu", dbService.menuForAccount(currentUser.getAccount().getId(), menuId));
			model.addAttribute("categories", dbService.menuCategories(currentUser.getAccount().getId(), menuId));
		}
		catch (DBException e)
		{
			logger.error("Error retrieving menu for account", e);
		}

		return "menuContent";
	}

	@RequestMapping("/menus/{menuId}/products/new")
	public String newProduct(@PathVariable Long menuId, @Valid @ModelAttribute ProductForm productForm, BindingResult result)
	{
		try
		{
			User currentUser = getCurrentUser();

			if (!result.hasErrors())
			{
				// Get the menu
				Menu menu = dbService.menuForAccount(currentUser.getAccount().getId(), menuId);
				if (menu != null)
				{

				}
			}
		}
		catch (DBException e)
		{
			logger.error("Error creating new product", e);
		}

		return "redirect:/menus";
	}
}