package com.zentive.naboo.util.shifts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;

import com.zentive.naboo.model.Shift;

public class ShiftTypeModel
{
	@Valid
	@Size(max = 5)
	private List<ShiftModel> mon = new ArrayList<>();

	@Valid
	@Size(max = 5)
	private List<ShiftModel> tue = new ArrayList<>();

	@Valid
	@Size(max = 5)
	private List<ShiftModel> wed = new ArrayList<>();

	@Valid
	@Size(max = 5)
	private List<ShiftModel> thu = new ArrayList<>();

	@Valid
	@Size(max = 5)
	private List<ShiftModel> fri = new ArrayList<>();

	@Valid
	@Size(max = 5)
	private List<ShiftModel> sat = new ArrayList<>();

	@Valid
	@Size(max = 5)
	private List<ShiftModel> sun = new ArrayList<>();

	public void addShift(Shift shift)
	{
		if (shift != null)
		{
			switch (shift.getDayOfWeek())
			{
			case 1:
				addShiftToDay(shift, mon);
				break;
			case 2:
				addShiftToDay(shift, tue);
				break;
			case 3:
				addShiftToDay(shift, wed);
				break;
			case 4:
				addShiftToDay(shift, thu);
				break;
			case 5:
				addShiftToDay(shift, fri);
				break;
			case 6:
				addShiftToDay(shift, sat);
				break;
			case 7:
				addShiftToDay(shift, sun);
				break;
			}
		}
	}

	public Boolean openAt(DateTime date)
	{
		Boolean open = false;

		// Get the supplied day
		Integer dayOfWeek = date.getDayOfWeek();
		Integer hourOfDay = date.getHourOfDay();

		List<ShiftModel> shifts = shiftListForDay(dayOfWeek);
		if (shifts != null)
		{
			for (ShiftModel shift : shifts)
			{
				// Is the shift enabled
				if (shift.getEnabled())
				{
					// Get hour and minutes
					LocalTime startTime = shift.getStartTimeAsLocalTime();
					LocalTime endTime = shift.getEndTimeAsLocalTime();

					// Id the supplied hour within the shifts opening hours?
					if (hourOfDay >= shift.getStartHour() && hourOfDay <= shift.getEndHour())
					{
						open = true;
						continue;
					}
				}
			}
		}

		return open;
	}

	private class ShiftModelComparator implements Comparator<ShiftModel>
	{
		@Override
		public int compare(ShiftModel s1, ShiftModel s2)
		{
			return s1.getStartHour().compareTo(s2.getStartHour());
		}
	}

	public void sort()
	{
		Collections.sort(mon, new ShiftModelComparator());
		Collections.sort(tue, new ShiftModelComparator());
		Collections.sort(wed, new ShiftModelComparator());
		Collections.sort(thu, new ShiftModelComparator());
		Collections.sort(fri, new ShiftModelComparator());
		Collections.sort(sat, new ShiftModelComparator());
		Collections.sort(sun, new ShiftModelComparator());
	}

	private List<ShiftModel> shiftListForDay(Integer day)
	{
		List<ShiftModel> shiftList = null;

		switch (day)
		{
		case 1:
			shiftList = mon;
			break;
		case 2:
			shiftList = tue;
			break;
		case 3:
			shiftList = wed;
			break;
		case 4:
			shiftList = thu;
			break;
		case 5:
			shiftList = fri;
			break;
		case 6:
			shiftList = sat;
			break;
		case 7:
			shiftList = sun;
			break;
		}

		return shiftList;
	}

	private void addShiftToDay(Shift shift, List<ShiftModel> shiftDay)
	{
		shiftDay.add(new ShiftModel(shift));
	}

	public List<ShiftModel> getMon()
	{
		return mon;
	}

	public void setMon(List<ShiftModel> mon)
	{
		this.mon = mon;
	}

	public List<ShiftModel> getTue()
	{
		return tue;
	}

	public void setTue(List<ShiftModel> tue)
	{
		this.tue = tue;
	}

	public List<ShiftModel> getWed()
	{
		return wed;
	}

	public void setWed(List<ShiftModel> wed)
	{
		this.wed = wed;
	}

	public List<ShiftModel> getThu()
	{
		return thu;
	}

	public void setThu(List<ShiftModel> thu)
	{
		this.thu = thu;
	}

	public List<ShiftModel> getFri()
	{
		return fri;
	}

	public void setFri(List<ShiftModel> fri)
	{
		this.fri = fri;
	}

	public List<ShiftModel> getSat()
	{
		return sat;
	}

	public void setSat(List<ShiftModel> sat)
	{
		this.sat = sat;
	}

	public List<ShiftModel> getSun()
	{
		return sun;
	}

	public void setSun(List<ShiftModel> sun)
	{
		this.sun = sun;
	}
}
